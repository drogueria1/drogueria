package mision.tic.mi_drogeria.vista;

import mision.tic.mi_drogeria.controlador.ControladorProducto;
import mision.tic.mi_drogeria.modelo.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class VistaProducto extends JFrame{
    private JPanel jPanel;
    private JTextField textFieldId;
    private JTextField textFieldNombre;
    private JTextField textFieldCantidad;
    private JTextField textFieldPrecio;
    private JButton agregarButton;
    private JButton actualizarButton;
    private JButton eliminarButton;
    private JButton consultarButton;

    public VistaProducto() throws HeadlessException {
        this.setTitle("Drogueria Uis");
        this.setSize(600,600);
        this.setContentPane(jPanel);
        this.setVisible(true);

        agregarProducto();
        actualizarProducto();
        eliminarProducto();
        seleccionarProducto();

    }

    @Autowired
    ControladorProducto controladorProducto;

    private void agregarProducto(){
        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombre = textFieldNombre.getText();
                int cantidad = Integer.parseInt(textFieldCantidad.getText());
                double precio = Double.parseDouble(textFieldPrecio.getText());

                boolean rta = controladorProducto.crearActualizarProducto(new Producto(nombre,cantidad,precio));

                if(rta)
                    System.out.println("Se agrego el producto");
                else
                    System.out.println("Error");
            }
        });
    }

    private void actualizarProducto(){
        actualizarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(textFieldId.getText());
                String nombre = textFieldNombre.getText();
                int cantidad = Integer.parseInt(textFieldCantidad.getText());
                double precio = Double.parseDouble(textFieldPrecio.getText());

                boolean rta = controladorProducto.crearActualizarProducto(new Producto(id,nombre,cantidad,precio));

                if(rta)
                    System.out.println("Se actualizo el producto");
                else
                    System.out.println("Error");
            }
        });
    }

    private void eliminarProducto(){
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(textFieldId.getText());

                boolean rta = controladorProducto.eliminarProductoPorId(id);

                if(rta)
                    System.out.println("Se elimino el producto");
                else
                    System.out.println("Error al eliminar");
            }
        });
    }

    private void seleccionarProducto(){
        consultarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(textFieldId.getText());
                Producto producto = controladorProducto.seleccionarProductoPorId(id);
                if(producto != null){
                    textFieldNombre.setText(producto.getNombre());
                    textFieldPrecio.setText(String.valueOf(producto.getPrecio()));
                    textFieldCantidad.setText(String.valueOf(producto.getCantidad()));
                }
                else
                    System.out.println("Error al eliminar");
            }
        });
    }

}

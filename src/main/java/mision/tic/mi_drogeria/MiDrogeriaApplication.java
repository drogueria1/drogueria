package mision.tic.mi_drogeria;

import mision.tic.mi_drogeria.vista.VistaProducto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class MiDrogeriaApplication {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(MiDrogeriaApplication.class)
				.headless(false)
				.run(args);


		VistaProducto vistaProducto = context.getBean(VistaProducto.class);
	}

}

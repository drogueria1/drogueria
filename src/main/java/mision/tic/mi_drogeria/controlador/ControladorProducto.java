package mision.tic.mi_drogeria.controlador;

import mision.tic.mi_drogeria.modelo.Producto;
import mision.tic.mi_drogeria.modelo.RepositorioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ControladorProducto {

    @Autowired
    RepositorioProducto repositorioProducto;



    public boolean crearActualizarProducto(Producto producto){
        try{
            repositorioProducto.save(producto);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarProductoPorId(int id){
        try{
            repositorioProducto.deleteById(id);
            return true;
        }catch (Exception e){
            System.err.println(e);
            return false;
        }
    }

    public Producto seleccionarProductoPorId(int id){
        try{
            return repositorioProducto.findById(id).get();
        }catch (Exception e){
            return null;
        }
    }

    public List<Producto> seleccionarTodosProducto(){
        try{
            return repositorioProducto.findAll();
        }catch (Exception e){
            return null;
        }
    }

}
